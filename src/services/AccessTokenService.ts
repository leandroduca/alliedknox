import * as bunyan from 'bunyan';
import * as knoxTokenLibraryJs from 'knox-token-library-js';

const log = bunyan.createLogger({ name: 'knox-acesstoken'});

export const getToken = async (client, clientDb, connection) => {

    let KeysResult = await clientDb.getKeys(connection);

    const signedClientId = knoxTokenLibraryJs.generateSignedClientIdentifierJWT(__dirname + '/../keys.json', KeysResult[1].ClientIdentifer);
    //const signedClientId = knoxTokenLibraryJs.generateSignedClientIdentifierJWT(__dirname + '/../keys.json', KeysResult[5].ClientIdentifer);
    const signedClientIdkdp = knoxTokenLibraryJs.generateSignedClientIdentifierJWT(__dirname + '/../kdp_keys.json', KeysResult[0].ClientIdentifer);

    const response = await client.getToken(signedClientId, KeysResult[1].PublicKey); 
    //const response = await client.getToken(signedClientId, KeysResult[5].PublicKey); 
    log.info(`Access Token KG: ${response.data.accessToken}`);

    const responsekdp = await client.getTokenkdp(signedClientIdkdp, KeysResult[0].PublicKey); 
    log.info(`Access Token KDP: ${responsekdp.data.accessToken}`);

    log.info('Assinatura do TOKEN KG');
    const signedtoken = knoxTokenLibraryJs.generateSignedAccessTokenJWT(__dirname + '/../keys.json', response.data.accessToken);
    
    log.info('Assinatura do TOKEN KDP');
    const signedtokenkdp = knoxTokenLibraryJs.generateSignedAccessTokenJWT(__dirname + '/../kdp_keys.json', responsekdp.data.accessToken);
 
    log.info('Inserção do TOKEN'); 
    const insertResponse = await clientDb.insertToken(connection, signedtoken, signedtokenkdp);
    log.info(`Token Inserido com sucesso ${insertResponse}`);
         
    return signedtoken;
}