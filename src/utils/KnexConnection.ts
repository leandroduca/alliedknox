import * as Knex from 'knex'


export const openConnection = async (connection) => {
    return Knex({
        client: 'mysql2',
        version: '5.7',
        connection: connection
    });
};

export const closeConnection = async (connection) => {
    await connection.destroy();
}