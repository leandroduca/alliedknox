import axios from 'axios'

export const getToken = async (signedClientId, clientPublic) => {
    //process.env.MYCLIENTPUBLIC
    return await axios.post(process.env.URLKNOXTOKEN + '/users/accesstoken', {
        base64EncodedStringPublicKey: clientPublic, 
        clientIdentifierJwt: signedClientId,  
        validityForAccessTokenInMinutes: process.env.EXPIRETOKEN || 30
    });
}

export const getTokenkdp = async (signedClientIdkdp, clientPublic) => {
    //process.env.MYCLIENTPUBLIC
    return await axios.post(process.env.URLKNOXTOKEN + '/users/accesstoken', {
        base64EncodedStringPublicKey: clientPublic, 
        clientIdentifierJwt: signedClientIdkdp,  
        validityForAccessTokenInMinutes: process.env.EXPIRETOKEN || 30
    });
}