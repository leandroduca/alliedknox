export const insertToken = async (conn, signedKg, signedKdp) => {
    return await conn('knoxPublicToken')
        .insert({ 
            "public_token_kg": signedKg,
            "public_token_kdp": signedKdp, 
            "date_time": conn.fn.now()
        });
}

export const getKeys = async (conn) => {
    return await conn('knoxAuthentication').select();
}