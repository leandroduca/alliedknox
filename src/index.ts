import * as bunyan from 'bunyan';
import * as knoxTokenLibraryJs from 'knox-token-library-js';
import * as accessTokenService from './services/AccessTokenService';
import * as client from './clients/client';
import * as clientDb from './clients/clientDb';
import * as KnexConnection from './utils/KnexConnection';

const log = bunyan.createLogger({ name: 'knox-acesstoken'});

module.exports.accesstoken = async(req: Request, res: Response) => {

    log.info('Iniciando a lambda function ACCESS TOKEN');
    
    log.info('Abre conexão com o banco');
    let connection = await KnexConnection.openConnection({host: process.env.DBHOST, user: process.env.DBUSER, port: process.env.DBPORT, password: process.env.DBPASSWORD, database:process.env.DBNAME});

    const accessToken = await accessTokenService.getToken(client, clientDb, connection);
    
    log.info('Fecha conexão com o banco');
    await KnexConnection.closeConnection(connection);
    
    log.info('Finalizando a lambda function ACCESS TOKEN');
    return { publictoken: accessToken };
}