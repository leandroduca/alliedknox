import {assert} from '../init';
import * as bunyan from 'bunyan';
import * as knoxTokenLibraryJs from 'knox-token-library-js';
import * as accessTokenService from '../../src/services/AccessTokenService';
import * as client from '../../src/clients/client';
import * as KnexConnection from '../../src/utils/KnexConnection';
 
process.env.DBHOST = 'alliedknox.cr6crxyd0okp.us-east-1.rds.amazonaws.com';
process.env.DBUSER = 'allied_knox';
process.env.DBPORT = '3306';
process.env.DBPASSWORD = 'allied-knox';
process.env.DBNAME = 'alliedknox_db';
process.env.URLKNOXTOKEN = 'https://us-kcs-api.samsungknox.com/ams/v1';
process.env.MYCLIENTPUBLIC = 'MIIBITANBgkqhkiG9w0BAQEFAAOCAQ4AMIIBCQKCAQBPZrE/ubIiE3AhH5ZJGiWCR17vE4KNbJdUooNg5qHVPqbhRpp2t7rR9IZxkP5eM5uCuZc0tE2fSOYODH1OPOV3W4FEyN/a+esXMpg7GF7e3e9L5RwnpwTHiyXRKrN4X99K8fosp9MOoQd/HNXEvHQ/H0fu53EZU3bQ3bR11cTH9eXy93T1UVI07HOowBbAhy/ohzBjjDJG+7SPk0G5u8suoPB9vJ2to5UVdhgo9MgPtxFSoB1eX6b3I8Mn+KFef6dOYsrwB1V/hRnj4R1Op0khBqSK1BkL8Dhq2WVqRstU3aFsV9gONJivHKshOsw2XNZp12W5ISUNnwQEb5gXdTPfAgMBAAE=';
const path = __dirname +'/../../resources/keys.json';
const MYCLIENTIDENTIFER = 'eyJhbGciOiJIUzUxMiJ9.eyJjbGllbnRJZGVudGlmaWVyIjoiNzAwOTI4N2EtYWI2MS00ZmNiLThhZjYtNGMwNGIzY2E2OWRiMTAxMjQyZWYtYzZkNi00ZWQyLThiZTAtOGNmYWNhYjJhMjc0IiwiYXR0cjEiOiIwIn0.DqfvZNenS7J8UyBeqyGT24JC_qJlOipYzat2sSIXuOwFR1d6AKwxKPwLvEOhhyeIKasXrSRIvkI2uEnlTUzqvw';


describe('AccessTokenServiceReturns',   () => {

    context('Get Return Token',  () => {
        it('AccessTokenService deve retornar um Token', async () => {
            
            const signedClientId = knoxTokenLibraryJs.generateSignedClientIdentifierJWT(path, MYCLIENTIDENTIFER);
            const response = await client.getToken(signedClientId);
            const signedtoken = knoxTokenLibraryJs.generateSignedAccessTokenJWT(path, response.data.accessToken);

            assert.isNotEmpty(signedtoken);
        });
    });

    context('Get Empty Token', () => {
        it('AccesTokenService não retorna um Token', async () => {
            await client.getToken(null).catch((err)  => {
                assert.equal(err.response.data.message,'RESOURCE_INVALID_PARAM','Não retornou token');
           });
        });
    });

/*
    it('AccessTokenService deve retornar erro (dados incorretos de conexão)', async () => {        
        try {
            let connection = await KnexConnection.openConnection(
                {host: process.env.DBHOST, 
                user: process.env.DBUSER, 
                port: process.env.DBPORT, 
                password: '#errorPassword', 
                database:process.env.DBNAME}
            )
            assert.notOk('Erro: dados incorretos.')
        }catch(err) {
            assert.isNotEmpty(err);
        };        
    });
*/
});

