import {assert} from '../init';
import 'mocha';
import { openConnection, closeConnection } from '../../src/utils/KnexConnection';

describe("KnexConnection", () => {
    context("Teste de conexão bem sucedida.", () => {
        let instance: any;

        it("Abre de conexão com o banco.",  async () => {
            instance =  await openConnection({
                host: "alliedknox.cr6crxyd0okp.us-east-1.rds.amazonaws.com", 
                user: "allied_knox", 
                port: 3306, 
                password: "allied-knox", 
                database: "alliedknox_db"  
            }).catch(() => {
                assert.notOk("Erro ao conectar.");
            });

            await instance.raw("select 1+1 as result").catch(() =>{
                assert.notOk("Erro ao conectar.");
            });
        });
        
        it("Fecha de conexão com o banco.", async () => {
            await closeConnection(instance).catch(() => {
                assert.notOk("Erro ao fechar conexão.");
            });
        });
    });

    context("Teste de conexão mal sucedida.", () => {
         
        it('Conexão deve retornar erro (dados incorretos de conexão)', async () => {        
            try {
                let connection = await openConnection(
                    {host: process.env.DBHOST, 
                    user: process.env.DBUSER, 
                    port: process.env.DBPORT, 
                    password: '#errorPassword', 
                    database:process.env.DBNAME}
                )                
            }catch(err) {
                assert.isNotEmpty(err);
                //assert.notOk('Erro: dados incorretos.');
            };        
        });
        
        /*
        let instance: any;
        
        it("Conexão com o banco.",  async () => {
            instance =  await openConnection({
                host: "knox-mysql.c0iegyexlzty.us-east-1.rds.amazonaws.com", 
                user: "admin", 
                port: 3302,
            }).catch(() => {
                assert.ok("Sucesso.")
            });
    
            await instance.raw("select 1+1 as result").then(() => {
                assert.notOk("Erro!");
            }, () => {
                assert.ok("Sucesso.");
            });
        });
        */
    });
});
