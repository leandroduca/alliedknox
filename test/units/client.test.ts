import {assert, expect, should, request} from '../init';
import {getToken,getTokenkdp} from '../../src/clients/client'
import * as knoxTokenLibraryJs from 'knox-token-library-js';
import * as sinon from 'sinon';

process.env.MYCLIENTIDENTIFER = 'eyJhbGciOiJIUzUxMiJ9.eyJjbGllbnRJZGVudGlmaWVyIjoiNzAwOTI4N2EtYWI2MS00ZmNiLThhZjYtNGMwNGIzY2E2OWRiMTAxMjQyZWYtYzZkNi00ZWQyLThiZTAtOGNmYWNhYjJhMjc0IiwiYXR0cjEiOiIwIn0.DqfvZNenS7J8UyBeqyGT24JC_qJlOipYzat2sSIXuOwFR1d6AKwxKPwLvEOhhyeIKasXrSRIvkI2uEnlTUzqvw'
process.env.URLKNOXTOKEN = 'https://us-kcs-api.samsungknox.com/ams/v1';
process.env.MYCLIENTPUBLIC = 'MIIBITANBgkqhkiG9w0BAQEFAAOCAQ4AMIIBCQKCAQBPZrE/ubIiE3AhH5ZJGiWCR17vE4KNbJdUooNg5qHVPqbhRpp2t7rR9IZxkP5eM5uCuZc0tE2fSOYODH1OPOV3W4FEyN/a+esXMpg7GF7e3e9L5RwnpwTHiyXRKrN4X99K8fosp9MOoQd/HNXEvHQ/H0fu53EZU3bQ3bR11cTH9eXy93T1UVI07HOowBbAhy/ohzBjjDJG+7SPk0G5u8suoPB9vJ2to5UVdhgo9MgPtxFSoB1eX6b3I8Mn+KFef6dOYsrwB1V/hRnj4R1Op0khBqSK1BkL8Dhq2WVqRstU3aFsV9gONJivHKshOsw2XNZp12W5ISUNnwQEb5gXdTPfAgMBAAE=';
process.env.MYCLIENTPUBLIC_KDP= 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiULZz71cymSWJBMHYYeCatoBniygRTq7uBUDdLhGB2rrv9qZ8XlxjXFM0L0WpN4/Vaos6du+lqM+s5hQDeSmogmelSnx0nfC04nqbU3T+YFDBaA8q0Bcge/yd0mb5zoLl/27n9Mh1Yhp2luZcr5Z76EOG1ha2FItaRaCf2YaiWS4YpFImV4ys++sEIFPn781db4pC9mJzZtjNPrrlhvEhRS+zMcQit4I3ftVIIDtw5REEhDQXEG2hYhuUYz7SXbKwd78CyFXtkr4547dvY+fQ8h0wTdo3/+NTg9tbquJ44RT3RxZ5tFC7m+aiIhp4VostLMHIkKOVUECzjh7RaW+nwIDAQAB';
const path = __dirname + '/../../resources/keys.json'

describe('getTokenTest', () =>{   
    context('getTokenValid', () => {
        it('getToken verificar se token foi gerado corretamente', async () => {

            let obj  = {getToken: getToken};
            let signedClientId = knoxTokenLibraryJs.generateSignedClientIdentifierJWT(path, process.env.MYCLIENTIDENTIFER); 
            const stub = sinon.stub(obj,'getToken');
            stub.returns(signedClientId);
            assert.isString(stub(), 'Token verdadeiro');
        });
    })

    context('getTokenInvalid', () => {
        it('getToken verificar se token está invalido', async () => {
            let signedClientId = '1223'; 
            await getToken(signedClientId).catch ((err) =>{
                assert.equal(err.response.data.message, 'AUTHENTICATION_FAIL', 'Token não é Valido');
            });

        });
    });
});

describe('getTokenkdp',() =>{
    context('getTokenkdpValid',() =>{
        it('getTokenkdp verificar se token foi gerado corretamente',async() =>{
            let obj  = {getTokenkdp: getTokenkdp};
            let signedClientIdkdp = knoxTokenLibraryJs.generateSignedClientIdentifierJWT(path, process.env.MYCLIENTPUBLIC_KDP); 
            const stub = sinon.stub(obj,'getTokenkdp');
            stub.returns(signedClientIdkdp);
            assert.isString(stub(), 'Token verdadeiro');
        });
    });
    context('getTokenkdpInvalid',()=>{
        it('getTokenkdp verificar se token está invalido', async() =>{
            let signedClientIdkdp = '1234';
            await getTokenkdp(signedClientIdkdp).catch ((err) =>{
                assert.equal(err.response.data.message, 'AUTHENTICATION_FAIL', 'Token não é Valido');
            });
        });
    });
});